# Reescribe el programa del salario usando try y except, de modo que el
# programa sea capaz de gestionar entradas no numéricas con elegancia,
# mostrando un mensaje y saliendo del programa.

# Autor: Brayan Gonzalo Cabrera Cabrera

# brayan.cabrera@unl.edu.ec

try:
    horas = int(input("introduzca las horas:  "))

    tarifa = float(input("Introduzca la tarifa por hora: "))

    if horas > 40:
        htarifa = horas - 40
        ex = (htarifa * 1.5) * tarifa
        salario = (40 * tarifa) + ex
        print("el salario básico es de: ", salario)
    else:
        salario = horas * tarifa
        print("el salario básico es de: ", salario)
except:
    print("Error, por favor ingrese un número")